<?php


namespace app\models;

use app\models\basic\GoodBasic;
use tuyakhov\jsonapi\ResourceInterface;
use tuyakhov\jsonapi\ResourceTrait;

class Good extends GoodBasic implements ResourceInterface
{
    use ResourceTrait;
}
