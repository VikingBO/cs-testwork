<?php

namespace app\models\basic;

use Yii;

/**
 * This is the model class for table "goods".
 *
 * @property int $id
 * @property string $title
 * @property string|null $image
 * @property int $price_int
 */
class GoodBasic extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'goods';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['price_int'], 'default', 'value' => null],
            [['price_int'], 'integer'],
            [['title', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'image' => 'Image',
            'price_int' => 'Price Int',
        ];
    }
}
