##Небольшое тестовое приложение с JSON API

###Настройка приложения на сервере
Для проверки необходим сервер с PHP 7+, composer, PostgreSQL 12

Так же в проекте есть docker-compose который поднимет
 сервер с нужным настройками.
 
Если приложение работает не из докера, то первоначально
после клонирования необходимо запустить

```composer install```

и настроить доступ к БД в файле ```config/db.php```

После установки необходимых зависимостей и настройки доступа, запускаем миграции   

```php yii migrate```

тем самым добавятся необходимые таблицы
в БД

Следующим шагом нам необходимо добавить тестовых данных в БД,
для этого из консоли находясь в папке проекта нужно выполнить

```php yii hello/goods-init```

---
###Работа с приложением
Если всё было выполнено верно и сервер запущен то мы можем работать с API приложения:

```GET /api/good``` - получить список товаров

```GET /api/good/<id>``` - получить информацию о товаре с идентификатором ```<id>```

```POST /api/good``` - добавить информацию о товаре

```PUT /api/good/<id>``` - изменить информацию о товаре с идентификатором```<id>```

Все запросы необходимо делать соблюдая спецификацию [JSON:API](https://jsonapi.org/format/)

Ответы также будут приходить в этом формате, включая системные ошибки.
