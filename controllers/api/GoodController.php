<?php


namespace app\controllers\api;


use app\models\Good;

class GoodController extends ApiController
{
    public $modelClass = Good::class;
}
