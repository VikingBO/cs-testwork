<?php

namespace app\controllers\api;


use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\Cors;
use yii\web\Response;

class ApiController extends \yii\rest\ActiveController
{
    public $enableCsrfValidation = false;
    public $serializer = [
        'class' => 'tuyakhov\jsonapi\Serializer',
        'pluralize' => false,
    ];

    public function beforeAction($action)
    {
        // Отключаем хранение сессии для API
        Yii::$app->user->enableSession = false;

        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['corsFilter'] = [
            'class' => Cors::class,
            'cors' => [
                'Origin' => ['127.0.0.1', 'localhost'],
                'Access-Control-Allow-Origin' => ['*'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Allow-Methods' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Allow-Headers' => [
                    'Authorization',
                    'Access-Control-Allow-Headers',
                    'Access-Control-Allow-Origin',
                    'Access-Control-Allow-Methods',
                    'Origin',
                    'Accept',
                    'Content-Type','content-type',
                    'X-Auth-Token',
                    'X-Requested-With'
                ],
            ],
        ];

        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/vnd.api+json' => Response::FORMAT_JSON,
            ],
        ];

        return $behaviors;
    }

    public function actions()
    {
        return [
            'index' => [
                'class' => 'tuyakhov\jsonapi\actions\IndexAction',
                'modelClass' => $this->modelClass
            ],
            'create' => [
                'class' => 'tuyakhov\jsonapi\actions\CreateAction',
                'modelClass' => $this->modelClass
            ],
            'update' => [
                'class' => 'tuyakhov\jsonapi\actions\UpdateAction',
                'modelClass' => $this->modelClass
            ],
            'view' => [
                'class' => 'tuyakhov\jsonapi\actions\ViewAction',
                'modelClass' => $this->modelClass
            ],
            'delete' => [
                'class' => 'tuyakhov\jsonapi\actions\DeleteAction',
                'modelClass' => $this->modelClass
            ],
            'options' => [
                'class' => 'yii\rest\OptionsAction',
            ],
        ];
    }


}
