<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\basic\GoodBasic;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";

        return ExitCode::OK;
    }

    /**
     * Добавляем тестовые данные по товарам
     */
    public function actionGoodsInit()
    {
        $this->stdout('start' . PHP_EOL);

        try {
            for ($i = 0; $i <= 9; ++$i) {
                $goods = new GoodBasic();
                $goods->title = 'Good_'.$i;
                $goods->image = 'https://previews.123rf.com/images/pavelstasevich/pavelstasevich1811/pavelstasevich181101065/112815953-no-image-available-icon-flat-vector.jpg';
                $goods->price_int = rand(0,100);
                $goods->save();
                $this->stdout('good save' . PHP_EOL);

                if ($goods->hasErrors()) {
                    $this->stdout(implode(PHP_EOL,$goods->getErrors()));
                } else {
                    $this->stdout('Item '.$goods->title.' created.' . PHP_EOL);
                }
            }

            return ExitCode::OK;
        } catch (\Exception $e) {
            $this->stdout($e->getMessage() . PHP_EOL);
            return ExitCode::CANTCREAT;
        }
    }
}
